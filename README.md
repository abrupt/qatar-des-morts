# ~/ABRÜPT/LAURENT MARGANTIN/QATAR DES MORTS/*

La [page de ce livre](https://abrupt.cc/laurent-margantin/qatar-des-morts/) sur le réseau.

## Sur le livre

Qatar 2010-2019: 6500 ouvriers migrants seraient morts sur les chantiers de la Coupe du monde de football. De mort naturelle selon les rapports officiels.

Qatar 2022 : finale de la Coupe du monde une semaine avant Noël. Estimation du marché télévisuel : 3,2 milliards de téléspectateurs.

La traduction, *Qatar of the dead*, est l'œuvre de [Bill Jenkinson](https://abrupt.cc/bill-jenkinson/).

## Sur l'auteur

Auteur et traducteur. Il écrit sur [Œuvres ouvertes](https://www.oeuvreouvertes.net), a publié notamment un récit (*Aux îles Kerguelen*, Éditions de la Revue des ressources), des poèmes (*Erres*, Tarmac éditions), ainsi qu'un essai (*Novalis ou l'écriture romantique*). Il travaille à une édition critique du Journal de Kafka.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).

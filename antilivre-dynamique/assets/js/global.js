import * as THREE from "./three/three.module.js";
import { MapControls } from "./three/OrbitControls.js";
// import { OrbitControls } from "./three/OrbitControls.js";
import { EffectComposer } from "./three/EffectComposer.js";
import { RenderPass } from "./three/RenderPass.js";
import { GlitchPass } from "./three/GlitchPass.js";
import { ShaderPass } from "./three/ShaderPass.js";
import { FilmPass } from "./three/FilmPass.js";
import { VignetteShader } from "../js/shaders/VignetteShader.js";

// Multilangue URL
let pathURL;
if (window.location.href.indexOf("/en/") != -1) {
  pathURL = "../";
} else {
  pathURL = "./";
}
//// Menu
const menuBtn = document.querySelector(".btn--menu");
const randomBtn = document.querySelector(".btn--random");
const menu = document.querySelector(".menu");
let menuOpen = false;
const musiqueBtn = document.querySelector(".btn--sound");
const musiqueBtnOn = document.querySelector(".btn--sound .info--on");
const musiqueBtnOff = document.querySelector(".btn--sound .info--off");
const morceau = document.querySelector(".son");

const loadBtn = document.querySelector(".btn--load");

loadBtn.addEventListener("click", (e) => {
  e.preventDefault();
  loadBtn.blur();
  document.querySelector('.loading').classList.add("loading--done");
});


menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuBtn.blur();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.enabled = false;
  } else {
    // if (!isTouchDevice()) controls.enabled = true;
  }
  menu.classList.toggle("show--menu");
});

// Musique
// let musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function () {
      if (track.volume < 0.99) {
        track.volume += 0.01;
      } else {
        clearInterval(fadeInAudio);
      }
    }, interval);
  } else {
    track.pause();
  }
}
let silence = true;
musiqueBtn.addEventListener("click", (e) => {
  e.preventDefault();
  if (silence) {
    musique(morceau);
  } else {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    morceau.pause();
  }
  musiqueBtnOn.classList.toggle("none");
  musiqueBtnOff.classList.toggle("none");
  silence = !silence;
  // musiqueBtn.classList.toggle('playing');
  musiqueBtn.blur();
});

function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomNbFrac(min, max) {
  // min and max included
  return Math.random() * (max - min + 1) + min;
}

function isTouchDevice() {
  return (
    "ontouchstart" in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0
  );
}

//
// Three.js
//
let container;
let camera, controls, scene, renderer;
let starBox, stars, vertices;
let ambientLight, dirLight1, dirLight2, spotLight;
let dirGroup;
let group;
let parameters;
let materials = [];
let composer, composer2;
let clickable = true;
let movingCamera = false;
let movingCameraEnd = false;
let animClick = false;
let loadingTexture1 = false;
let loadingTexture2 = false;
let loadingTexture3 = false;
let loadingTexture4 = false;
let loadingTexture5 = false;
let loadingTexture6 = false;
let loadingTexture7 = false;
let loadingTextureGround = false;
let loadingBlocks = false;
let everythingLoaded = false;

const textes = [...document.querySelectorAll(".texte")];
const texteContainer = document.querySelector(".textes");

let listSolids = [];

const mouse = new THREE.Vector2();
const clock = new THREE.Clock();

init();
//render(); // remove when using next line for animation loop (requestAnimationFrame)
animate();

function init() {
  scene = new THREE.Scene();

  // Colors
  scene.background = new THREE.Color(0xd0a96d);
  scene.fog = new THREE.Fog(0xd0a96d, 1, 1000);

  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    4000
  );
  camera.position.set(200, 25, 500);

  // lights
  ambientLight = new THREE.AmbientLight(0xfaf5ef);
  scene.add(ambientLight);

  // spotLight = new THREE.SpotLight(0xfb2ead);
  // spotLight.angle = Math.PI / 5;
  // spotLight.penumbra = 0.3;
  // spotLight.position.set(5, 10, 5);
  // spotLight.castShadow = true;
  // spotLight.shadow.camera.near = 8;
  // spotLight.shadow.camera.far = 2000;
  // spotLight.shadow.mapSize.width = 512;
  // spotLight.shadow.mapSize.height = 512;
  // spotLight.shadow.bias = -0.002;
  // spotLight.shadow.radius = 4;
  // scene.add(spotLight);

  dirLight1 = new THREE.DirectionalLight(0xf4ebdc, 0.02);
  dirLight1.position.set(-100, 100, 100);
  dirLight2 = new THREE.DirectionalLight(0xe0c69f, 0.1);
  dirLight2.position.set(100, 100, -100);
  // dirLight1.castShadow = true;
  // dirLight1.shadow.camera.near = 1;
  // dirLight1.shadow.camera.far = 400;
  // dirLight1.shadow.camera.right = 200;
  // dirLight1.shadow.camera.left = -200;
  // dirLight1.shadow.camera.top = 150;
  // dirLight1.shadow.camera.bottom = -150;
  // dirLight1.shadow.mapSize.width = 1024;
  // dirLight1.shadow.mapSize.height = 1024;
  // dirLight1.shadow.radius = 4;
  // dirLight1.shadow.bias = -0.0009;

  dirGroup = new THREE.Group();
  // dirGroup.add(dirLight1);
  dirGroup.add(dirLight2);
  scene.add(dirGroup);

  // const helper = new THREE.CameraHelper(dirLight1.shadow.camera);
  // scene.add(helper);
  // const axesHelper = new THREE.AxesHelper(5000);
  // axesHelper.setColors(0xff0000, 0x00ff00, 0x0000ff);
  // scene.add(axesHelper);
  // const helperLight = new THREE.DirectionalLightHelper(dirLight1, 5);
  // scene.add(helperLight);
  // const helperLight2 = new THREE.DirectionalLightHelper(dirLight2, 5);
  // scene.add(helperLight2);

  // Textures
  const mapConcrete1 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete1.jpg",
    function (texture) {
      loadingTexture1 = true;
    }
  );
  mapConcrete1.wrapS = mapConcrete1.wrapT = THREE.RepeatWrapping;
  mapConcrete1.anisotropy = 16;

  const mapConcrete2 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete2.jpg",
    function (texture) {
      loadingTexture2 = true;
    }
  );
  mapConcrete2.wrapS = mapConcrete2.wrapT = THREE.RepeatWrapping;
  mapConcrete2.anisotropy = 16;

  const mapConcrete3 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete3.jpg",
    function (texture) {
      loadingTexture3 = true;
    }
  );
  mapConcrete3.wrapS = mapConcrete3.wrapT = THREE.RepeatWrapping;
  mapConcrete3.anisotropy = 16;

  const mapConcrete4 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete4.jpg",
    function (texture) {
      loadingTexture4 = true;
    }
  );
  mapConcrete4.wrapS = mapConcrete4.wrapT = THREE.RepeatWrapping;
  mapConcrete4.anisotropy = 16;

  const mapConcrete5 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete5.jpg",
    function (texture) {
      loadingTexture5 = true;
    }
  );
  mapConcrete5.wrapS = mapConcrete5.wrapT = THREE.RepeatWrapping;
  mapConcrete5.anisotropy = 16;

  const mapConcrete6 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete6.jpg",
    function (texture) {
      loadingTexture6 = true;
    }
  );
  mapConcrete6.wrapS = mapConcrete6.wrapT = THREE.RepeatWrapping;
  mapConcrete6.anisotropy = 16;

  const mapConcrete7 = new THREE.TextureLoader().load(
    pathURL + "js/textures/concrete7.jpg",
    function (texture) {
      loadingTexture7 = true;
    }
  );
  mapConcrete7.wrapS = mapConcrete7.wrapT = THREE.RepeatWrapping;
  mapConcrete7.anisotropy = 16;

  const mapSable = new THREE.TextureLoader().load(
    pathURL + "js/textures/sable.jpg",
    function (texture) {
      loadingTextureGround = true;
    }
  );
  mapSable.wrapS = mapSable.wrapT = THREE.RepeatWrapping;
  mapSable.anisotropy = 16;
  mapSable.repeat.set(60, 60);

  // ground
  const planeGeometry = new THREE.PlaneGeometry(2000, 2000, 8, 8);
  const planeMaterial = new THREE.MeshLambertMaterial({
    color: 0xd6b480,
    map: mapSable,
    // shininess: 0,
    // specular: 0x653f2c,
  });

  const ground = new THREE.Mesh(planeGeometry, planeMaterial);
  ground.rotation.x = -Math.PI / 2;
  ground.position.set(0, 0, 0);
  ground.scale.multiplyScalar(3);
  // ground.castShadow = true;
  // ground.receiveShadow = true;
  scene.add(ground);

  // Rains
  const geometry = new THREE.BufferGeometry();
  const vertices = [];

  const textureLoader = new THREE.TextureLoader();

  const sprite1 = textureLoader.load(pathURL + "js/textures/particle.png");

  for (let i = 0; i < 100; i++) {
    const x = Math.random() * 2000 - 1000;
    const y = Math.random() * 2000 - 1000;
    const z = Math.random() * 2000 - 1000;

    vertices.push(x, y, z);
  }

  geometry.setAttribute(
    "position",
    new THREE.Float32BufferAttribute(vertices, 3)
  );

  parameters = [
    [[1.0, 0.2, 0.5], sprite1, 20],
    [[0.95, 0.1, 0.5], sprite1, 15],
    [[0.9, 0.05, 0.5], sprite1, 10],
    [[0.85, 0, 0.5], sprite1, 8],
    [[0.8, 0, 0.5], sprite1, 5],
  ];

  for (let i = 0; i < parameters.length; i++) {
    const color = parameters[i][0];
    const sprite = parameters[i][1];
    const size = 10;

    materials[i] = new THREE.PointsMaterial({
      size: size,
      color: 0x000000,
      map: sprite,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true,
    });
    // materials[i].color.setHSL(color[0], color[1], color[2]);

    const particles = new THREE.Points(geometry, materials[i]);

    particles.rotation.x = Math.random() * 6;
    particles.rotation.y = Math.random() * 6;
    particles.rotation.z = Math.random() * 6;

    // scene.add(particles);
  }

  // starBox = new THREE.BufferGeometry();
  // vertices = {
  //   positions: [],
  //   accelerations: [],
  //   velocities: [],
  // };
  //
  // for (let i = 0; i < 18000; i++) {
  //   vertices.positions.push(Math.random() * 600 - 300);
  //   if (i % 3 === 0) {
  //     vertices.accelerations.push(0);
  //     vertices.velocities.push(0.2);
  //   }
  // }
  //
  // starBox.setAttribute(
  //   "position",
  //   new THREE.BufferAttribute(new Float32Array(vertices.positions), 3)
  // );
  //
  // const starImage = new THREE.TextureLoader().load(
  //   "./js/textures/particle.png"
  // );
  //
  // let starMaterial = new THREE.PointsMaterial({
  //   color: 0xb38339,
  //   size: 7,
  //   // map: starImage,
  // });
  //
  // stars = new THREE.Points(starBox, starMaterial);
  //
  // scene.add(stars);

  // Objects
  // Grid of objects
  let grid = [];
  const nbSolids = 6500;
  const columns = 65;
  const rows = 100;
  const factor = 5;
  const gapX = factor * 0.95;
  const gapY = factor * 0.95;
  const objSizeX = factor * 0.95;
  const objSizeY = factor * 2.38;
  const sizeCellX = objSizeX + gapX;
  const sizeCellY = objSizeY + gapY;
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
      grid.push({
        occupy: 0,
        x: j * sizeCellX - Math.floor(columns / 2) * sizeCellX + objSizeX / 2,
        y: i * sizeCellY - Math.floor(rows / 2) * sizeCellY + objSizeY / 2,
      });
    }
  }

  const boxGeometry = new THREE.BoxGeometry(1, 1, 1);
  const boxMaterial = [
    new THREE.MeshLambertMaterial({
      map: mapConcrete1,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete2,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete3,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete4,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete5,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete6,
      color: 0xd6b480,
    }),
    new THREE.MeshLambertMaterial({
      map: mapConcrete7,
      color: 0xd6b480,
    }),
  ];

  for (let i = 0; i < nbSolids; i++) {
    const obj = new THREE.Mesh(boxGeometry, boxMaterial[randomNb(0, boxMaterial.length - 1)]);
    const cell = grid[i];
    const randomHeight = randomNbFrac(factor * 0.65, factor * 4.7);
    obj.scale.x = objSizeX;
    obj.scale.y = randomHeight;
    obj.scale.z = objSizeY;
    let posX = cell.x;
    let posY = randomHeight / 2;
    let posZ = cell.y;
    obj.position.set(posX, posY, posZ);
    // obj.updateMatrix();
    // obj.matrixAutoUpdate = false;
    // obj.castShadow = true;
    // obj.receiveShadow = true;
    listSolids.push(obj);
    scene.add(obj);

    if (i == nbSolids - 1) {
      loadingBlocks = true;
    }

    // renderer.render(scene, camera);
  }

  // for (let i = 0; i < 500; i++) {
  //   const mesh = new THREE.Mesh(geometry, material);
  //   mesh.scale.x = 20;
  //   mesh.scale.y = Math.random() * 80 + 10;
  //   mesh.scale.z = 20;
  //   mesh.updateMatrix();
  //   mesh.matrixAutoUpdate = false;
  //   scene.add(mesh);
  // }

  // const dirLight1 = new THREE.DirectionalLight(0xffffff);
  // dirLight1.position.set(1, 1, 1);
  // scene.add(dirLight1);

  // const dirLight2 = new THREE.DirectionalLight(0x002288);
  // dirLight2.position.set(-1, -1, -1);
  // scene.add(dirLight2);

  // const ambientLight = new THREE.AmbientLight(0x222222);
  // scene.add(ambientLight);

  //

  // Render
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  // renderer.shadowMap.enabled = true;
  // renderer.shadowMap.type = THREE.VSMShadowMap;

  // controls
  controls = new MapControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.screenSpacePanning = false;
  // controls.panSpeed = 0.2;
  // controls.rotateSpeed = 0.8;
  // controls.zoomSpeed = 0.8;
  controls.minDistance = 0;
  controls.maxDistance = 1200;
  controls.maxPolarAngle = Math.PI / 2.015;
  controls.target = new THREE.Vector3(0, 1, 0);
  controls.update();

  // Container
  container = document.getElementById("container");
  container.appendChild(renderer.domElement);

  // postprocessing
  const shaderVignette = VignetteShader;
  const effectVignette = new ShaderPass(shaderVignette);
  const glitchPass = new GlitchPass();
  // glitchPass.goWild = true;

  effectVignette.uniforms["offset"].value = 0.9;
  effectVignette.uniforms["darkness"].value = 1.75;

  const effectFilm = new FilmPass(0.3, 0.05, 800, false);

  // Glitch
  composer = new EffectComposer(renderer);
  composer.addPass(new RenderPass(scene, camera));

  composer = new EffectComposer(renderer);
  composer.addPass(new RenderPass(scene, camera));

  composer.addPass(glitchPass);
  composer.addPass(effectFilm);
  composer.addPass(effectVignette);

  // Permanent
  composer2 = new EffectComposer(renderer);
  composer2.addPass(new RenderPass(scene, camera));

  composer2.addPass(effectFilm);
  composer2.addPass(effectVignette);

  window.addEventListener("resize", onWindowResize);
}

let timer;
controls.addEventListener("start", (el) => {
  movingCameraEnd = false;
  // timer = setTimeout(() => {
  //   moveControls = true;
  // }, 150);
});

controls.addEventListener("end", (el) => {
  clearTimeout(timer);
  movingCameraEnd = true;
  if (movingCamera) {
    movingCamera = false;
  }
});

controls.addEventListener("change", (el) => {
  if (movingCameraEnd) {
    clearTimeout(timer);
    movingCamera = false;
  } else {
    timer = setTimeout(() => {
      movingCamera = true;
    }, 150);
  }
});

container.addEventListener("click", onDocumentMouseClick, false);
container.addEventListener("mousemove", onDocumentMouseMove, false);
texteContainer.addEventListener(
  "mousemove",
  (event) => {
    event.stopPropagation();
    document.body.style.cursor = "";
  },
  false
);

let textesOpen = false;
let timerGlitch;
function onDocumentMouseClick(event) {
  const mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  if (intersects.length > 0 && clickable) {
    clearTimeout(timerGlitch);
    const texteChoisi = textes[randomNb(0, textes.length - 1)];
    document.body.style.cursor = "";
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      texteChoisi.classList.add("show");
      document.querySelector(".texte.show").scrollIntoView();
    } else {
      texteContainer.classList.add("show");
      texteChoisi.classList.add("show");
      // if (!isTouchDevice()) controls.activeLook = false;
      textesOpen = !textesOpen;
    }
    animClick = true;
    timerGlitch = setTimeout(() => {
      animClick = false;
    }, 2000);
  }
}

const closeBtn = document.querySelector(".textes__close");
closeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".texte.show").scrollIntoView();
  texteContainer.classList.remove("show");
  textes.forEach((el) => {
    el.classList.remove("show");
  });
  textesOpen = !textesOpen;
  // if (!isTouchDevice()) controls.activeLook = true;
  closeBtn.blur();
});

function onDocumentMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";
  } else {
    document.body.style.cursor = "";
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate(time) {
  requestAnimationFrame(animate);
  controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true
  render();

  if (
    !everythingLoaded &&
    loadingTexture1 &&
    loadingTexture2 &&
    loadingTexture3 &&
    loadingTexture4 &&
    loadingTexture5 &&
    loadingTexture6 &&
    loadingTexture7 &&
    loadingTextureGround &&
    loadingBlocks
  ) {
    everythingLoaded = true;
    const finish = document.querySelector(".loading");
    finish.classList.add("loading--all-is-loaded");
  }

  if (movingCamera) {
    clickable = false;
  } else {
    clickable = true;
  }

  for (let i = 0; i < scene.children.length; i++) {
    const object = scene.children[i];

    const speedParticles = Date.now() * 0.0005;
    if (object instanceof THREE.Points) {
      object.rotation.y = speedParticles * (i < 4 ? i + 1 : -(i + 1));
    }
  }

  for (let i = 0; i < materials.length; i++) {
    // const color = parameters[i][0];
    //
    // const h = ((360 * (color[0] + time)) % 360) / 360;
    // materials[i].color.setHSL(h, color[1], color[2]);
  }

  // for (let i = 0; i < vertices.velocities.length; i++) {
  //   vertices.velocities[i / 3 + (i % 3)] += vertices.accelerations[i];
  //   vertices.positions[i * 3 + 1] -= vertices.velocities[i];
  //
  //   if (vertices.positions[i * 3 + 1] < -200) {
  //     vertices.positions[i * 3 + 1] = 4000;
  //     vertices.velocities[i / 3 + (i % 3)] = 0;
  //   }
  // }

  // stars.rotation.y += 0.002;

  // starBox.setAttribute(
  //   "position",
  //   new THREE.BufferAttribute(new Float32Array(vertices.positions), 3)
  // );

  // const delta = clock.getDelta() / 20;
  //
  // dirGroup.rotation.y += 0.7 * delta;
  // dirLight.position.z = 17 + Math.sin(time * 0.001) * 5;


  if (animClick) {
    composer.render();
  } else {
    composer2.render();
  }
}

function render() {
  renderer.render(scene, camera);
}

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector(".menu"));
let simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
// Disable scroll simplebar for print
window.addEventListener("beforeprint", (event) => {
  simpleBarContent.unMount();
  simpleBarContentTextes.unMount();
});
window.addEventListener("afterprint", (event) => {
  simpleBarContent = new SimpleBar(document.querySelector(".menu"));
  simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
});

//
// Scripts hack
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

////
//// Prevent bounce effect iOS
////
//// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
//// let contentBounce = document.querySelector('.container');
//// contentBounce.addEventListener('touchstart', function (event) {
////   this.allowUp = this.scrollTop > 0;
////   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
////   this.slideBeginY = event.pageY;
//// });

//// contentBounce.addEventListener('touchmove', function (event) {
////   let up = event.pageY > this.slideBeginY;
////   let down = event.pageY < this.slideBeginY;
////   this.slideBeginY = event.pageY;
////   if ((up && this.allowUp) || (down && this.allowDown)) {
////     event.stopPropagation();
////   } else {
////     event.preventDefault();
////   }
//// });

// //
// // Old iOS versions
// //
// let messageIOS = false;
//
// const platform = window.navigator.platform;
// const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
//
// if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
//   const banner = document.createElement('div');
//   banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">This page will not work properly with iOS versions older than 13.<br>(Click on this banner to make it disappear.)</p></div>`;
//   banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
//   document.querySelector('body').appendChild(banner);
//   messageIOS = true;
// }

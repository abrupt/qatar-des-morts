---
title: Qatar des morts
---

<div class="texte">

[1.]  We make death invisible.

</div>
<div class="texte">

[2.]  Constructions en conteneurs tertiaires pour installations sportives en acier galvanisé modulables.

</div>
<div class="texte">

[3.]  J'écris mes articles climatisés dans une salle climatisée.

</div>
<div class="texte">

[4.]  Votre passeport vous sera rendu à votre retour, que vous soyez vivant ou mort.

</div>
<div class="texte">

[5.]  Quelques hommes fument à l'ombre d'un abri en tôle.

</div>
<div class="texte">

[6.]  Un mélange d'hyperthermie, d'épuisement et de déshydratation.

</div>
<div class="texte">

[7.]  La forme du stade Al-Thumama est inspirée du couvre-chef traditionnel et le stade Al-Wakrah ressemble à un vagin.

</div>
<div class="texte">

[8.]  La Qatar Foundation verse 35 000 euros par mois au théologien musulman Tariq Ramadan.

</div>
<div class="texte">

[9.]  Terrain de 20 hectares au milieu de nulle part, une "base vie" pour 3 700 ouvriers.

</div>
<div class="texte">

[10.] La société Qatar Steel Company (QASCO) fut créée le 14 octobre 1974 par le décret numéro 130 de l'Émir. Il s'agissait d'une coentreprise associant le gouvernement du Qatar (70 %) et deux sociétés japonaises, Kobe Steel (20 %) et Tokyo Boeki (10 %).

</div>
<div class="texte">

[11.] --- Tu les as vus, tous ces morts ? --- Non, personne ne les a vus, ils sont dans les journaux.

</div>
<div class="texte">

[12.] Dans son bureau climatisé, le cadre supérieur de Qatari Diar Vinci Construction (QDVC) porte une petite laine.

</div>
<div class="texte">

[13.] Jeudi 28 mars 2019, le Premier ministre français Édouard Philippe a visité le stade Al-Wakrah construit spécialement pour la coupe du monde de football 2022 au Qatar.

</div>
<div class="texte">

[14.] La Qatar Foundation qui verse 35 000 euros tous les mois à Tariq Ramadan est présidée par Cheikha Mozah, la mère de l'actuel émir du Qatar.

</div>
<div class="texte">

[15.] Je suis journaliste dans un grand journal dit de référence en France, je suis payé pour faire des résumés.

</div>
<div class="texte">

[16.] Housses mortuaires avec poignées (*sacs cadavre*).

</div>
<div class="texte">

[17.] Au début, je cochais tous les morts sur le chantier dans un tableau Excel, puis j'ai arrêté.

</div>
<div class="texte">

[18.] Au nord de Doha, construction d'une gare souterraine à 35 mètres de profondeur.

</div>
<div class="texte">

[19.] Le Premier ministre français Édouard Philippe a signé un accord avec le Qatar qui "vise à construire un partenariat stratégique pour la préparation de la Coupe du monde en 2022 et la gestion de la sécurité de l'événement".

</div>
<div class="texte">

[20.] Installer l'air conditionné dans un stade à ciel ouvert: un défi que le Qatar a su relever.

</div>
<div class="texte">

[21.] À l'aéroport, vous serez accueillis par une personne qui vous conduira jusqu'au bus et à qui vous confierez votre passeport.

</div>
<div class="texte">

[22.] On exagère beaucoup la chaleur au Qatar. D'abord la Coupe du monde 2022 aura lieu en hiver, et puis il y a la clim partout.

</div>
<div class="texte">

[23.] Conditions de travail chez Qatari Diar Vinci Construction (QDVC) : 8 heures par jour + 2 heures supplémentaires, soit 60 heures par semaine, salaire : 450 euros par mois en moyenne. Salaire minimum au Qatar en 2017 : 180 euros par mois.

</div>
<div class="texte">

[24.] Ils sont morts de chaleur, ils travaillaient à la construction d'un stade climatisé.

</div>
<div class="texte">

[25.] Ce jour-là, il ne voulait pas retourner au chantier, je vous le dis, il ne voulait pas.

</div>
<div class="texte">

[26.] Détenue à 51 % par le fonds souverain qatari Diar et à 49 % par Vinci, QDVC a engrangé pour 5 milliards d'euros de contrats au Qatar depuis 2007.

</div>
<div class="texte">

[27.] Une gamme de housses mortuaires biodégradables en conformité avec les textes réglementaires et adaptées aux besoins actuels.

</div>
<div class="texte">

[28.] Sur son compte Twitter, Édouard Philippe a précisé que "l'ensemble des savoir-faire en matière de logistique, d'infrastructure, de sécurité est mobilisé pour la prochaine Coupe du monde de football au Qatar".

</div>
<div class="texte">

[29.] Tariq Ramadan a rapatrié 590 000 euros en provenance de son compte qatari alimenté par les versements mensuels de la Qatar Foundation.

</div>
<div class="texte">

[30.] Les transferts de fonds auraient servi à l'achat d'un duplex situé rue Gabrielle sur la butte Montmartre à Paris.

</div>
<div class="texte">

[31.] Transformer un tas de sable en gratte-ciel, marinas et centres commerciaux.

</div>
<div class="texte">

[32.] Qatar Steel produit de l'acier : des aciers plats, des aciers longs, de l'acier en barres, de l'acier inoxydable, des câbles, de la tôle forte.

</div>
<div class="texte">

[33.] Nasser al-Khelaïfi, président du PSG, mis en examen pour "corruption active" dans l'affaire des mondiaux d'athlétisme au Qatar.

</div>
<div class="texte">

[34.] Convoqué par le juge Renaud van Ruymbeke, Nasser al-Khelaïfi ne s'est pas présenté, invoquant sa présence à la finale de la Coupe du Qatar de football, et a été mis en examen par courrier.

</div>
<div class="texte">

[35.] En 2007, QASCO dévoile son nouveau nom Qatar Steel et son slogan: "We make Steel Matter".

</div>
<div class="texte">

[36.] Notre métier est de satisfaire les besoins de nos clients. Nous réalisons des camps de travail clé en main conformes aux projets requis.

</div>
<div class="texte">

[37.] Une installation du sculpteur américain Richard Serra a été inaugurée en plein désert.

</div>
<div class="texte">

[38.] Le Qatar a signé des contrats de plus de 10 milliards d'euros lors d'une visite d'Emmanuel Macron à Doha le 7 décembre 2017.

</div>
<div class="texte">

[39.] "East-West/West-East" de l'artiste américain Richard Serra est une commande de la sœur de l'émir, Sheikha al-Mayassa, qui veut faire du pays un grand centre artistique.

</div>
<div class="texte">

[40.] Je collectionne des posters de foot depuis l'âge de 5 ans, j'en ai un bon millier. Certains sont même dédicacés.

</div>
<div class="texte">

[41.] Les morts sont souvent âgés d'une vingtaine d'années. Leur certificat de décès parle de "crise cardiaque" ou de "défaillance respiratoire".

</div>
<div class="texte">

[42.] Veiller à bien mettre le passeport du mort à l'intérieur de la housse mortuaire (*note de service*).

</div>
<div class="texte">

[43.] J'aime le foot depuis toujours, les histoires de corruption de dirigeants de la FIFA ne m'intéressent pas. J'ai un rapport purement esthétique au football. Le reste, je le laisse, ça pue.

</div>
<div class="texte">

[44.] Parmi les contrats signés lors de la visite du président français au Qatar : la concession du métro de Doha et du tramway de Lusail à un consortium SNCF/RATP.

</div>
<div class="texte">

[45.] Ouvriers indiens, népalais ou bangladais mobilisés dans une chaleur de four et la poussière du désert.

</div>
<div class="texte">

[46.] Des dizaines attablés à l'intérieur d'une vaste tente légèrement climatisée.

</div>
<div class="texte">

[47.] La température ressentie peut dépasser les 50 degrés l'été.

</div>
<div class="texte">

[48.] On m'a dit : tu pars à Doha, tu vas faire un résumé de la situation sur les chantiers de Vinci.

</div>
<div class="texte">

[49.] J'ai toujours été doué pour faire des résumés, des résumés de résumés, des résumés de résumés de résumés.

</div>
<div class="texte">

[50.] L'installation composée de quatre plaques d'acier de 15 mètres de haut est située à 60 kilomètres de Doha et n'est accessible par aucune route asphaltée.

</div>
<div class="texte">

[51.] L'artiste américain Richard Serra a déclaré: "Je me suis rendu dans plusieurs endroits et j'ai aimé ce désert."

</div>
<div class="texte">

[52.] Évidemment, sur le terrain, pour la sérénité des joueurs et pour qu'ils puissent jouer à leur meilleur niveau, on maintiendra une température à 26 degrés et une température agréable pour les spectateurs.

</div>
<div class="texte">

[53.] Vérifier l'identité du mort (*étiquette attachée à un pied*) avant de mettre le passeport à l'intérieur de la housse mortuaire (*note de service*).

</div>
<div class="texte">

[54.] 100 000 Népalais sont venus travailler au Qatar en 2012.

</div>
<div class="texte">

[55.] Les ouvriers ne disposent pas d'aires de repos et font la queue sous le soleil lors de la distribution des repas.

</div>
<div class="texte">

[56.] La nourriture est de mauvaise qualité, on sert des fruits pourris. Des ouvriers ont souffert de maladies digestives.

</div>
<div class="texte">

[57.] À cause de la chaleur, des ouvriers vomissent et tombent par terre.

</div>
<div class="texte">

[58.] Quel gazon dans les stades pour le Qatar où les températures peuvent dépasser les 40 degrés ?

</div>
<div class="texte">

[59.] "Ces plaques connectent les mers situées à l'est et à l'ouest de ce paysage", a ajouté l'artiste américain Richard Serra dont les œuvres sont exposées à Doha.

</div>
<div class="texte">

[60.] Sheikha al-Mayassa dirige l'Autorité des musées du Qatar (QMA). Elle aspire à faire du richissime émirat gazier du Golfe un centre artistique majeur.

</div>
<div class="texte">

[61.] Sheikha al-Mayassa dépense à la tête de la QMA environ un milliard de dollars sur le marché de l'art.

</div>
<div class="texte">

[62.] Installation rapide des structures d'acier et de l'isolation thermique des bâtiments. Toutes ces unités préfabriquées sont démontées et facilement emballées pour expédition.

</div>
<div class="texte">

[63.] La pelouse d'un stade doit être plane, lisse, suffisamment humide pour permettre la glisse.

</div>
<div class="texte">

[64.] Dans le cadre de la Coupe du monde de football au Qatar, les pelouses seront toutes réalisées en gazon naturel.

</div>
<div class="texte">

[65.] Il suffira de renforcer les brins d'herbe. Le gazon naturel sera enraciné dans un substrat composé de liège, de sable fin et de microfibres synthétiques dans lequel le gazon s'ancre très fortement en s'enroulant autour des fibres.

</div>
<div class="texte">

[66.] 44 ouvriers employés sur les chantiers de la Coupe du monde de football 2022 seraient morts entre juin et août 2013.

</div>
<div class="texte">

[67.] Pendant l'été 2013, les ouvriers népalais seraient morts au rythme d'un par jour.

</div>
<div class="texte">

[68.] Mais attention, car sur ce genre de pelouse les joueurs, en cas de chute ou de glissade, peuvent se brûler.

</div>
<div class="texte">

[69.] Quand je suis quelque part, je parle à peine du lieu, je cherche juste les éléments qui peuvent entrer dans mon article.

</div>
<div class="texte">

[70.] Le monde se résume pour moi à des blocs de mots que j'agence avec le plus d'objectivité possible.

</div>
<div class="texte">

[71.] Il faut savoir qu'une pelouse représente autour de 1 % du prix de construction d'un stade.

</div>
<div class="texte">

[72.] Travail forcé, refus d'accès à l'eau potable, pourtant gratuite, en plein cœur du désert.

</div>
<div class="texte">

[73.] La violoniste Anne Gravoin, épouse à l'époque du Premier ministre français Manuel Valls, s'est produite le 30 mars 2015 à Doha en présence du ministre de la Culture du Qatar, Hamad ben Abdelaziz al-Kawari.

</div>
<div class="texte">

[74.] Mes blocs de mots sont mieux au froid, dans un bureau climatisé.

</div>
<div class="texte">

[75.] Les unités préfabriquées peuvent être facilement soulevées par des grues et/ou des chariots élévateurs. Toutes les unités assemblées ont quatre crochets de coin installés sur le toit.

</div>
<div class="texte">

[76.] Les polymères remplissent les stades de football.

</div>
<div class="texte">

[77.] Au Qatar, ce sont des polymères encore plus résistants, dont les brins, après avoir été enduits de silicone, sont plantés dans un mélange de polypropylène expansé et de granules de caoutchouc synthétique généralement issu de vieux pneus.

</div>
<div class="texte">

[78.] Lusail City, à 17 kilomètres de Doha, est l'un des plus grands chantiers du monde, avec une population prévue de 250 000 habitants pour lesquels on construit des appartements de luxe.

</div>
<div class="texte">

[79.] C'est incroyable ce qu'ils ont fait depuis que je suis ici. Il y a treize ans, ce n'était que du sable. On dirait que c'est fait au crayon à papier.

</div>
<div class="texte">

[80.] Produits: 33 600 m^2^ SureGrip PE 100 AGRV, 3 mm - 24 000 m^2^ membranes d'étanchéité AGRV, 3 mm - fil de soudure AGRV, section circulaire, 4 mm.

</div>
<div class="texte">

[81.] Travail non rémunéré, des conditions d'hébergement insalubres avec parfois jusqu'à 12 employés entassés dans une seule chambre.

</div>
<div class="texte">

[82.] Ils travaillent l'estomac vide pendant 24 heures. 12 heures de travail et pas de nourriture pendant toute la nuit.

</div>
<div class="texte">

[83.] Le polyamide a fait place au polypropylène ou au polyéthylène.

</div>
<div class="texte">

[84.] Réservoirs d'eau potable pour ville de luxe au Qatar.

</div>
<div class="texte">

[85.] Membranes d'étanchéité et panneaux de protection du béton pour réservoirs d'eau potable.

</div>
<div class="texte">

[86.] Entre le 4 juillet et le 24 août 2014, plus de 50 patients souffrant de maladies causées par une chaleur extrême ont été admis à l'hôpital général Hamad.

</div>
<div class="texte">

[87.] Une population d'une telle envergure requiert une grande quantité d'eau potable. Des réservoirs d'eau souterrains avec une capacité de 150 000 m^3^ chacun sont en cours de construction.

</div>
<div class="texte">

[88.] Un tweet de Qatar Airways: "Which teams will be playing the opening game of CONMEBOL 2019 in Brazil ? Venezuela - Peru / Brazil - Bolivia / Qatar - Colombia / Japan - Chile ?"

</div>
<div class="texte">

[89.] Les patients souffraient de dysfonctionnements du système nerveux se traduisant par un comportement étrange, un état mental altéré, des hallucinations. Cinq d'entre eux étaient dans le coma.

</div>
<div class="texte">

[90.] Les stades seront couverts d'une surface transparente, un toit en verre ne pouvant être installé, pour une simple raison de poids étant donné l'espace à couvrir.

</div>
<div class="texte">

[91.] Au Qatar, 90 % de la main-d'œuvre est composée de migrants venus d'Asie du sud.

</div>
<div class="texte">

[92.] Je n'aime pas trop écrire de longues enquêtes, je préfère écrire des articles courts: tant de morts, telles sommes d'argent, tel jour et tel lieu, des données brutes et pas de blabla autour.

</div>
<div class="texte">

[93.] Les ouvriers souffraient de graves insolations et ont été admis dans un état critique à l'Intensive Care Unit (ICU).

</div>
<div class="texte">

[94.] Systèmes d'étanchéité en matière plastique de grande qualité de la société AGRU Kunststofftechnick GmbH (Autriche).

</div>
<div class="texte">

[95.] Quand je suis allé à Doha, c'était juste un aller-retour pour interroger les responsables de Vinci et d'organisations humanitaires sur les conditions de travail des ouvriers.

</div>
<div class="texte">

[96.] Les stades seront couverts d'un tissu de fibre de verre enduit de PTFE (poly-tétra-fluor-éthylène), d'une toile qui résiste aux températures extrêmes.

</div>
<div class="texte">

[97.] Ma vie ici, c'est comme la prison. Le travail est difficile, nous travaillons pendant de longues heures en plein soleil.

</div>
<div class="texte">

[98.] Quand je me suis plaint de ma situation peu de temps après mon arrivée au Qatar, le manager a dit: "Si tu veux te plaindre, tu peux, mais cela aura des conséquences. Si tu veux rester au Qatar, sois tranquille et continue à travailler."

</div>
<div class="texte">

[99.] PTFE, ETFE, deux polymères chouchous des architectes pour l'élégance qu'ils donnent au stade de football.

</div>
<div class="texte">

[100.] Quelques gouttes de pluie suffisent à nettoyer une toile enduite de PTFE couvrant un stade et celle-ci peut avoir une durée de vie de 20 ans. Cette même toile reflète par ailleurs 73 % de l'énergie solaire.

</div>
<div class="texte">

[101.] L'ancien Premier ministre Dominique de Villepin devenu avocat d'affaires travaille pour l'entreprise Veolia dont un fonds qatari détient 5 % du capital.

</div>
<div class="texte">

[102.] L'épineux dossier des conditions de travail.

</div>
<div class="texte">

[103.] Devant la Loi, il y a un manager.

</div>
<div class="texte">

[104.] Après le sport, le Qatar a fait de l'art son nouveau vecteur de communication.

</div>
<div class="texte">

[105.] L'ancien Premier ministre français Dominique de Villepin dispose d'un bureau à la Qatar Museum Authority dirigée par son ancienne étudiante Sheikha al-Mayassa.

</div>
<div class="texte">

[106.] Dominique de Villepin est très proche de la famille qui gouverne le Qatar, les Al-Thani. Il a été le tuteur de Sheikha al-Mayassa, la sœur de l'émir, quand elle est venue étudier à Paris. Celle-ci qualifie Dominique de Villepin de "deuxième père".

</div>
<div class="texte">

[107.] Chaque migrant travaillant au Qatar doit avoir un "parrain", lequel doit être également son employeur.

</div>
<div class="texte">

[108.] Les migrants travaillant au Qatar doivent avoir la permission de leur parrain pour changer de travail ou quitter le pays.

</div>
<div class="texte">

[109.] On pense que Dominique de Villepin "conseille" le fonds personnel de la mère de Sheikha al-Mayassa, la Cheikha Moza, qui s'est notamment offert le maroquinier Le Tanneur.

</div>
<div class="texte">

[110.] Le cimentier Vicat, associé au groupe Vinci, a mis au point le BCV (béton compatible Vicat).

</div>
<div class="texte">

[111.] Tu en as assez ? Tu veux en finir ? Monte dans le bus alors.

</div>
<div class="texte">

[112.] Chaque migrant se voit confisquer son passeport par son parrain.

</div>
<div class="texte">

[113.] 15 bâtiments de deux étages en tôle couleur sable.

</div>
<div class="texte">

[114.] Un bureau de l'OIT (Organisation internationale du travail) ouvert en avril 2018 à Doha sur un projet de "recrutement équitable".

</div>
<div class="texte">

[115.] Sur les chantiers nous ne le savions pas, mais nous étions déjà morts.

</div>
<div class="texte">

[116.] Avions climatisés, aéroports climatisés, taxis climatisés, chambres d'hôtel climatisées, stades climatisés.

</div>
<div class="texte">

[117.] Je passe des nuits entières à réécrire mes articles jusqu'à ce que mes petits blocs de mots soient parfaitement taillés et forment tous ensemble un grand bloc de mots.

</div>
<div class="texte">

[118.] Nos morts voyagent beaucoup. Nos morts choisissent Qatar Airways. Qatar Airways, le confort des morts.

</div>
<div class="texte">

[119.] Concernant le désert du Qatar, je ne vais pas parler de la chaleur ressentie, mais tâcher d'établir le nombre de morts par la chaleur. D'ailleurs, je n'ai jamais voyagé au Qatar.

</div>
<div class="texte">

[120.] Pour écrire des articles de journaux, il vaut mieux garder la tête froide, on envoie de moins en moins de gens sur le terrain.

</div>
<div class="texte">

[121.] Mohamed Jaham Al-Kuwari a été ambassadeur du Qatar à Paris entre 2003 et 2013. Pendant ces dix années, il a corrompu d'innombrables hommes politiques français en leur faisant de nombreux cadeaux.

</div>
<div class="texte">

[122.] Les plus corrompus par l'ambassadeur du Qatar étaient les membres du groupe d'amitié France-Qatar à l'Assemblée nationale. Ils recevaient des montres Rolex ou des bons d'achat dans les grands magasins pouvant aller jusqu'à 5 000 ou 6 000 euros.

</div>
<div class="texte">

[123.] Kafka a écrit *Le Château* en 1922, il y a exactement un siècle.

</div>
<div class="texte">

[124.] Les sept patients admis à l'hôpital souffraient d'insuffisance rénale.

</div>
<div class="texte">

[125.] Ils souffraient également de graves insolations et ont été transférés à l'ICU (Intensive Care Unit) dans un état critique.

</div>
<div class="texte">

[126.] Les épouses des hommes politiques corrompus par l'ambassadeur du Qatar Mohammed al-Kuwari pouvaient quant à elles recevoir des sacs Vuitton d'une valeur de 5 000 euros ou d'autres articles de grande marque.

</div>
<div class="texte">

[127.] Juin 2019, canicule en France. En Ille-et-Vilaine, enquête ouverte après la mort d'un couvreur de 33 ans alors qu'il faisait plus de 35 degrés à l'ombre.

</div>
<div class="texte">

[128.] Tu es vivant ? Tu es mort ? Est-ce que tu m'entends ? Est-ce que tu me vois ?

</div>
<div class="texte">

[129.] Mohammed Jaham al-Kuwari a offert des chaussures de marque au député français Nicolas Bays, élu socialiste du Nord-Pas-de-Calais, membre du groupe d'amitié France-Qatar à l'Assemblée nationale.

</div>
<div class="texte">

[130.] Commander désormais des housses mortuaires couleur sable, les noires sont trop visibles (*note de service*).

</div>
<div class="texte">

[131.] À sa fête de départ, Mohammed Jaham al-Kuwari a accueilli dans son hôtel particulier sur les quais de Seine tous ceux et toutes celles qu'il avait corrompus par le passé, pour la plupart des hommes politiques et des représentants du show business.

</div>
<div class="texte">

[132.] La filiale de Vinci, QDVC, est la première à avoir organisé des élections de représentants du personnel, en l'absence de syndicats, interdits au Qatar.

</div>
<div class="texte">

[133.] À Doha, le musée national du Qatar construit par l'architecte français Jean Nouvel représente une "Rose des sables". On estime qu'il a coûté 434 millions de dollars.

</div>
<div class="texte">

[134.] Le béton fibré à ultra-hautes performances est un nouveau matériau de construction qui atteint par optimisation des recettes et l'adjonction de fibres métalliques une résistance six fois plus élevée que le béton normal avec une durabilité elle aussi plus élevée.

</div>
<div class="texte">

[135.] Housse mortuaire biodégradable en Plastylon Link de couleur blanche à glissière nylon sur la face avant, double curseur.

</div>
<div class="texte">

[136.] La Rose des Sables : 539 disques d'acier de 14 à 87 m de diamètre, recouverts par 115 000 m^2^ de béton fibré ultra-hautes performances.

</div>
<div class="texte">

[137.] En admirant la Rose des sables de Jean Nouvel lors de son inauguration, je me suis demandé combien d'ouvriers étaient morts sur le chantier.

</div>
<div class="texte">

[138.] Matière biodégradable, le Plastylon Link est dans l'air du temps, car elle respecte l'environnement en se dégradant entièrement en moins de 8 mois.

</div>
<div class="texte">

[139.] "J'ai voulu créer une architecture évoquant la géographie et, selon la tradition du lieu, un bâtiment préservé au maximum du soleil", écrit l'architecte Jean Nouvel sur son blog.

</div>
<div class="texte">

[140.] Juin 2019, canicule en France. À Cernay dans le Haut-Rhin, un homme âgé de 36 ans a été retrouvé inanimé sur un chantier à côté d'une pelleteuse.

</div>
<div class="texte">

[141.] À partir des années 1990, une véritable rupture s'est produite dans le développement des connaissances sur le béton avec la mise au point de nouveaux concepts sur la formulation des matrices cimentaires, l'utilisation des fibres et l'optimisation des empilements granulaires.

</div>
<div class="texte">

[142.] Le Plastylon Link a été créé en 1989 comme matériau biodégradable pour garniture de cercueil et housse d'ensevelissement. Ce produit est notamment utilisé dans les cercueils destinés à la crémation puisque composé de matières combustibles et sublimables.

</div>
<div class="texte">

[143.] "Le Musée national du Qatar émerge d'un désert qui s'est aventuré dans la mer", écrit Jean Nouvel à propos de sa Rose des sables.

</div>
<div class="texte">

[144.] Lors du cocktail qui suivit l'inauguration de la Rose des sables, l'architecte Jean Nouvel et Sheikha al-Mayassa, la sœur de l'émir qui dirige l'Autorité des musées du Qatar, bavardaient ensemble comme de vieux amis.

</div>
<div class="texte">

[145.] En cas d'enterrement, le film du Plastylon Link est imperméable à l'eau pendant un mois, il est ensuite attaqué par l'hydrolise ainsi que par les bactéries et se décompose, permettant ainsi la dégradation du corps placé à l'intérieur.

</div>
<div class="texte">

[146.] "Prendre la rose des sables comme point de départ devient une idée très progressiste, pour ne pas dire utopiste", écrit Jean Nouvel.

</div>
<div class="texte">

[147.] Dans ma chambre d'hôtel climatisée, j'écris des blocs de mots climatisés : "Haute silhouette au crâne toujours rasé, toujours habillé de noir, Jean Nouvel aime parler de son dernier bébé."

</div>
<div class="texte">

[148.] La peau du bâtiment a été réalisée en béton fibré à ultra-hautes performances, d'une couleur beige sable qui est la même à l'extérieur et à l'intérieur du bâtiment.

</div>
<div class="texte">

[149.] Jean Nouvel s'est défendu de travailler pour des régimes non démocratiques : "Je travaille à l'échelle du siècle ou des siècles, pour les peuples, pas pour une personne ponctuellement au pouvoir."

</div>
<div class="texte">

[150.] Le corps se désagrège rapidement puisque le Plastylon Link contient peu de matériaux plastiques pour une majorité d'éléments biodégradables.

</div>
<div class="texte">

[151.] Lors du cocktail qui suivit l'inauguration de la Rose des sables, j'ai vu Jean Nouvel et Jack Lang qui bavardaient ensemble. Je l'ai noté, mais ne l'ai pas repris dans mon article car le détail était insignifiant.

</div>
<div class="texte">

[152.] Nos housses mortuaires ne sont pas talquées, mais très souples, de ce fait elles sont agréables à la manipulation. De plus, l'esthétique rendue par la texture et la couleur blanche de la housse offrent une certaine sobriété.

</div>
<div class="texte">

[153.] La gamme des BFUHP (bétons fibrés à ultra-hautes performances) permet d'obtenir des textures très variées et des parements lisses satinés, mats, brillants, homogènes et très réguliers présentant d'excellentes qualités esthétiques.

</div>
<div class="texte">

[154.] Une fois que la housse puis le corps à l'intérieur se sont décomposés, il ne reste plus aucune trace de l'une et de l'autre.

</div>
<div class="texte">

[155.] La Rose des sables est la première architecture autocréée par la nature, par le vent, les embruns, le sable et les millénaires, elle est d'une complexité et d'une poésie surprenantes.

</div>
<div class="texte">

[156.] Actuellement, nous travaillons à un nouveau modèle de housse mortuaire biodégradable accélérant la décomposition du corps à l'intérieur, effaçant toutes les traces en à peine quelques semaines.

</div>

---
title: Qatar of the dead
---

<div class="texte">

[1.] We make death invisible.

</div>
<div class="texte">

[2.] Modular galvanised-steel containerised buildings for sports facilities.

</div>
<div class="texte">

[3.] I write my air-conditioned articles in an air-conditioned room.

</div>
<div class="texte">

[4.] Your passport will be handed back to you on your return, whether alive or dead.

</div>
<div class="texte">

[5.] A few men are smoking in the shade of a sheet-metal shelter.

</div>
<div class="texte">

[6.] A combination of hypothermia, exhaustion, and dehydration.

</div>
<div class="texte">

[7.] The design of the Al-Thumama stadium is based on the shape of a traditional head-dress and the Al-Wakrah stadium looks like a vagina.

</div>
<div class="texte">

[8.] The Qatar Foundation transfers 35,000 euros a month to the Muslim theologian Tariq Ramadan.

</div>
<div class="texte">

[9.] 20 hectares of land in the middle of nowhere, “living accommodation” for 3,700 workers.

</div>
<div class="texte">

[10.] The Emir created the Qatar Steel Company (QSCO) by decree number 130 dated 14^th^ October 1974. It was a joint enterprise between the Qatar government (70%) and two Japanese firms: Kobe Steel (20%) and Tokyo Boeki (10%).

</div>
<div class="texte">

[11.] Did you see all those dead people? No, no one's seen them: they're in the newspapers.

</div>
<div class="texte">

[12.] In his air-conditioned office, the senior manager from Qatari Diar Vinci Construction (QDVC) is wearing a light woollen sweater.

</div>
<div class="texte">

[13.] On Thursday 28^th^ March 2019, the French Prime Minister Édouard Philippe visited the Al-Wakrah stadium, built specially for the 2022 World Cup Finals in Qatar.

</div>
<div class="texte">

[14.] Sheikha Moza, mother of the ruling Emir of Qatar, chairs the Qatar Foundation which transfers 35,000 euros a month to Tariq Ramadan.

</div>
<div class="texte">

[15.] I am a journalist for a major newspaper, recognised as a newspaper of record in France, they pay me to summarise facts.

</div>
<div class="texte">

[16.] Cadaver pouches with handles (*body bags*).

</div>
<div class="texte">

[17.] At first, I entered everyone who died onsite into an Excel Spreadsheet, then I stopped.

</div>
<div class="texte">

[18.] North of Doha, an underground station is under construction 35 metres below the surface.

</div>
<div class="texte">

[19.] Édouard Philippe, the French Prime Minister, signed an agreement with Qatar which “aims to build a strategic partnership to prepare for the 2022 World Cup and manage security for the event”.

</div>
<div class="texte">

[20.] Installing air-conditioning in an open-air stadium: a challenge which Qatar has been able to meet.

</div>
<div class="texte">

[21.] Someone will meet you at the airport and take you to a bus: you will hand over your passport.

</div>
<div class="texte">

[22.] They make far too much of the heat in Qatar. Anyway, the 2022 World Cup will be played in winter and there is air-con everywhere

</div>
<div class="texte">

[23.] Working conditions at Qatar Diar Vinci Construction (QDVC): 8-hour day + 2 hours overtime, 60 hours a week, 450 euros per month average salary. Qatar minimum wage in 2017: 180 euros a month.

</div>
<div class="texte">

[24.] They died from the heat: they were working on the construction of an air-conditioned stadium.

</div>
<div class="texte">

[25.] On that day, he refused to return onsite: I tell you, he refused.

</div>
<div class="texte">

[26.] 51% owned by the Diar Qatari Sovereign Wealth Fund and 49% by Vinci, QDVC has raked in 5 billion euros worth of contracts in Qatar since 2007.

</div>
<div class="texte">

[27.] A range of biodegradable body bags to meet regulatory norms and adapted to current needs.

</div>
<div class="texte">

[28.] On his Twitter Feed, Édouard Philippe announced that “we are mobilising our joint capabilities as regards logistics, infrastructure and security for the coming Football World Cup in Qatar”.

</div>
<div class="texte">

[29.] Tariq Ramadan sent 590,000 euros home from his Qatari bank account thanks to the monthly transfers from the Qatar Foundation.

</div>
<div class="texte">

[30.] These transferred funds may have been used to buy a duplex apartment on rue Gabrielle on the Butte Montmartre in Paris.

</div>
<div class="texte">

[31.] Transforming a heap of sand into skyscrapers, marinas, and shopping centres.

</div>
<div class="texte">

[32.] Qatar Steel produces steel: steel plate, rolled steel, steel bars, stainless steel, steel cables, reinforced sheet steel.

</div>
<div class="texte">

[33.] Nasser Al-Khelaifi, President of PSG, indicted for bribery in the World Athletics scandal in Qatar.

</div>
<div class="texte">

[34.] Summonsed by Judge Renaud van Ruymbeke, Nasser Al-Khelaifi did not appear, pleading his presence at the finals of the Qatar Football Cup, and was indicted by post.

</div>
<div class="texte">

[35.] In 2007, QASCO unveiled its new name Qatar Steel and its strapline: “We make Steel Matter”.

</div>
<div class="texte">

[36.] Our role is to meet the needs of our clients. We are building workers' accommodation ready for handover in conformity with current projects.

</div>
<div class="texte">

[37.] An installation by the American sculptor Richard Serra has been inaugurated in the open desert.

</div>
<div class="texte">

[38.] Qatar signed contracts worth more than 10 billion euros when Emmanuel Macron visited Doha on December 7^th^, 2017.

</div>
<div class="texte">

[39.] “East-West/West-East” by American artist Richard Serra was commissioned by the Emir's sister, Sheikha Al-Mayassa, who intends to turn the country into a major artistic centre.

</div>
<div class="texte">

[40.] I have collected football posters since I was five years old, I have at least a thousand. Some are even autographed.

</div>
<div class="texte">

[41.] The dead are often in their twenties. Their death certificates mention “heart attack” or “respiratory failure”.

</div>
<div class="texte">

[42.] Be careful to place the passport inside the body bag (*memo*).

</div>
<div class="texte">

[43.] I have always loved football. Stories about corruption among top FIFA people don't interest me. My relationship with football is purely aesthetic. I don't have anything to do with the rest of it, it stinks.

</div>
<div class="texte">

[44.] Amongst the contracts signed when the French president visited Qatar: the Doha metro concession and the Lusail tramway awarded to an SNCF/RATP consortium.

</div>
<div class="texte">

[45.] Indian, Nepalese, or Bangladeshi workers deployed into oven-like heat and desert dust conditions.

</div>
<div class="texte">

[46.] There were dozens of them seated inside a barely air-conditioned tent.

</div>
<div class="texte">

[47.] Temperatures can rise to over 50 degrees in summer.

</div>
<div class="texte">

[48.] They told me: you are going to Doha and will report your findings on the situation in the Vinci construction sites.

</div>
<div class="texte">

[49.] I have always been good at writing reports, reports on reports, reports on reports on reports.

</div>
<div class="texte">

[50.] The installation which consists of four 15-metre-high steel plates is sited 60 kilometres from Doha and cannot be reached by asphalt road.

</div>
<div class="texte">

[51.] The American artist Richard Serra stated: “I have visited several places and I liked this desert”.

</div>
<div class="texte">

[52.] Obviously, on the pitch, in the interests of player comfort so they can play at their best level, ambient temperature will be kept at 26 degrees and the temperature for spectators will be acceptable.

</div>
<div class="texte">

[53.] Confirm the dead man's identity (*find label attached to his foot*) before placing his passport inside the body bag (*memo*).

</div>
<div class="texte">

[54.] 100,000 Nepalese came to work in Qatar in 2012.

</div>
<div class="texte">

[55.] Workers had no access to rest areas and queued in full sunshine when meals were served.

</div>
<div class="texte">

[56.] Food quality is bad; they serve up rotten fruit. Workers suffered from digestive ailments

</div>
<div class="texte">

[57.] Because of the heat, workers vomit and fall to the ground.

</div>
<div class="texte">

[58.] What grass for stadiums in Qatar with temperatures above 40 degrees?

</div>
<div class="texte">

[59.] “These plates connect seas to the east and west of this landscape” added American artist Richard Serra whose work is exhibited in Doha.

</div>
<div class="texte">

[60.] Sheikha Al-Mayassa runs the Qatar Museums Authority (QMA). She aims to make the hyper-rich natural gas-producing Emirate a major artistic centre.

</div>
<div class="texte">

[61.] Sheikha Al-Mayassa spends about a billion dollars a year on the art market as the director of the QMA.

</div>
<div class="texte">

[62.] Rapid completion of steel structures and thermal insulation of buildings. All prefabricated units can be disassembled and easily packaged for despatch.

</div>
<div class="texte">

[63.] The pitch in a stadium must be level, soft and humid enough to allow for sliding.

</div>
<div class="texte">

[64.] For the Football World Cup in Qatar, all pitches will consist of natural grass.

</div>
<div class="texte">

[65.] Strengthening the grass blades will be good enough. We will root natural grass in a substrate of cork, fine sand and synthetic microfibre to which the grass will attach itself securely by wrapping itself around the fibres.

</div>
<div class="texte">

[66.] 44 workers employed on the 2022 Football World Cup construction sites may have died between June and August 2013.

</div>
<div class="texte">

[67.] During summer 2013, Nepalese workers may have died at the rate of one every day.

</div>
<div class="texte">

[68.] Take care, as players can burn themselves if they fall or slide on this type of pitch.

</div>
<div class="texte">

[69.] When I go somewhere, I hardly talk about the place, I only look for material to go into my article.

</div>
<div class="texte">

[70.] For me, the world consists of word-blocks which I arrange as objectively as I can.

</div>
<div class="texte">

[71.] You need to know that the pitch is responsible for about 1% of the cost of building a stadium.

</div>
<div class="texte">

[72.] Forced labour, without access to drinking water in the middle of the desert, even though it's free.

</div>
<div class="texte">

[73.] Anne Gravoin, concert violinist and wife of Manuel Valls, the then French Prime Minister, performed on 30^th^ March 2015 in Doha in the presence of Hamad Bin Abdulaziz Al-Kawari, Qatar Minister of Culture.

</div>
<div class="texte">

[74.] My word-blocks are better kept cool in an air-conditioned office.

</div>
<div class="texte">

[75.] It is easy to hoist prefabricated units by crane or forklift truck. All units have hooks at each corner of the roof.

</div>
<div class="texte">

[76.] Polymers fill football stadiums.

</div>
<div class="texte">

[77.] In Qatar we use high-resistance polymers with silicon-coated blades planted in a mixture of expanded polystyrene and synthetic rubber granules made from old tyres.

</div>
<div class="texte">

[78.] Lusail City, 17 kilometres from Doha, is one of the largest building sites in the world, with a forecast population of 250,000 residents for whom luxury apartments are under construction.

</div>
<div class="texte">

[79.] It's incredible what they've done since I came here. Only thirteen years ago, this was just sand. It feels like it has all been coloured in with crayons.

</div>
<div class="texte">

[80.] Materials: 33,600 square metres SureGrip PE 100 AGRV, 3mm - 24,000 square metres sealing membrane AGRV, 3mm - welding rod AGRV, circular section, 4mm.

</div>
<div class="texte">

[81.] Unpaid work, insanitary lodging conditions with up to 12 employees in a single room.

</div>
<div class="texte">

[82.] They work on empty stomachs for 24 hours. 12 hours at work and then no food overnight.

</div>
<div class="texte">

[83.] Polyamide has given way to polypropylene or polyethylene.

</div>
<div class="texte">

[84.] Drinking water reservoirs for luxury residences in Qatar.

</div>
<div class="texte">

[85.] Sealing membranes and protection panels for concrete drinking water reservoirs.

</div>
<div class="texte">

[86.] Between 4^th^ July and 24^th^ August 2014, more than 50 patients suffering from conditions caused by extreme heat were admitted to Hamad general hospital.

</div>
<div class="texte">

[87.] A population of such a considerable size requires a large amount of drinking water. Underground water reservoirs each able to hold 150,000 cubic metres are under construction.

</div>
<div class="texte">

[88.] Qatar Airways tweeted: “Which teams will be playing the opening game of CONMEBOL 2019 in Brazil? Venezuela - Peru / Brazil - Bolivia / Qatar - Colombia / Japan - Chile?”

</div>
<div class="texte">

[89.] The patients were suffering from dysfunctions of the nervous system resulting in strange behaviour, altered mental states and hallucinations. Five of them were comatose.

</div>
<div class="texte">

[90.] Stadiums will have roofs in transparent material, as glass roofs are not viable for such big spaces, due to their weight.

</div>
<div class="texte">

[91.] In Qatar, 90% of the workforce consists of migrants from south Asia.

</div>
<div class="texte">

[92.] I don't really like writing long investigative pieces, I prefer short articles: so many dead, so much money spent, on such and such a date and in such and such a place, just the facts without any waffle.

</div>
<div class="texte">

[93.] Workers suffered severely from sunstroke and were admitted to intensive care (ICU) in a critical condition.

</div>
<div class="texte">

[94.] Lining systems in high quality plastic material from AGRU Kunststofftechnik GMBH Austria.

</div>
<div class="texte">

[95.] When I went to Doha, it was just a round trip to interview the Vinci management and humanitarian organisations about conditions for workers.

</div>
<div class="texte">

[96.] The stadiums will be roofed in glass fibre material coated in PTFE (Poly-tetra-fluoro-ethylene), resistant to extremely high temperatures.

</div>
<div class="texte">

[97.] My life here is like prison. Work is hard, we work long hours in full sun.

</div>
<div class="texte">

[98.] When I complained about my conditions shortly after arriving in Qatar, the manager told me: “If you want to complain you can, but there will be consequences. If you want to stay in Qatar, keep quiet and keep working.”

</div>
<div class="texte">

[99.] PTFE, ETFE, two favourite polymers with architects because they make stadiums look elegant.

</div>
<div class="texte">

[100.] A few drops of rain are enough to clean fabric sealed in PTFE covering a stadium with a life expectancy of 20 years. This material also reflects 73% of solar energy.

</div>
<div class="texte">

[101.] Former Prime Minister Dominique de Villepin, now a commercial lawyer, works for Veolia, 5% owned by a Qatari Fund.

</div>
<div class="texte">

[102.] The thorny dossier on working conditions.

</div>
<div class="texte">

[103.] In the eyes of the law, there is a manager.

</div>
<div class="texte">

[104.] After sport, Qatar has made art its latest communications medium.

</div>
<div class="texte">

[105.] Former French Prime Minister Dominique de Villepin has an office in the Qatar Museum Authority directed by his former student Sheikha Al-Mayassa.

</div>
<div class="texte">

[106.] Dominique de Villepin is very close to the ruling family of Qatar, the Al-Thani. He tutored Sheikha Al-Mayassa, the Emir's sister, when she came to study in Paris. She calls Dominique de Villepin her 'second father'.

</div>
<div class="texte">

[107.] Every migrant working in Qatar must have a sponsor who must also be his employer.

</div>
<div class="texte">

[108.] Migrants working in Qatar must have their sponsor's permission to change jobs or leave the country.

</div>
<div class="texte">

[109.] Some people think that Dominique de Villepin is personal financial adviser to Sheikha Moza, Al-Mayassa's mother, who bought the leather goods business Le Tanneur.

</div>
<div class="texte">

[110.] The cement manufacturer Vicat which is part of the Vinci group developed VCC (Vicat Compatible Concrete).

</div>
<div class="texte">

[111.] Had enough? Want to end it? Well get on the bus then.

</div>
<div class="texte">

[112.] Every migrant has his passport confiscated by his sponsor.

</div>
<div class="texte">

[113.] 15 two-storey buildings in sand-coloured sheet metal.

</div>
<div class="texte">

[114.] An ILO (International Labour Office) opened in April 2018 in Doha to deliver “fair trade recruitment”.

</div>
<div class="texte">

[115.] On the sites, although we didn't know it, we were already dead.

</div>
<div class="texte">

[116.] Airconditioned aircraft, airconditioned airports, airconditioned taxis, airconditioned hotel rooms, airconditioned stadiums.

</div>
<div class="texte">

[117.] I spend entire nights rewriting my articles until each text-block is perfectly formed and they all combine into a big text-block.

</div>
<div class="texte">

[118.] Our dead travel a lot. Our dead choose Qatar Airways. Qatar Airways, the airline dead people prefer.

</div>
<div class="texte">

[119.] As far as the desert in Qatar goes, I won't talk about the heat that people experience, but I will try to establish the numbers dying from heat exhaustion. In fact, I have never been to Qatar.

</div>
<div class="texte">

[120.] For newspaper articles, it is better to keep a cool head, you find fewer and fewer people in the field.

</div>
<div class="texte">

[121.] Mohammed Jaham Al Kuwari was Qatar's Ambassador in Paris from 2003 to 2013. During this ten-year period, he corrupted many French politicians by offering them large numbers of gifts.

</div>
<div class="texte">

[122.] The Qatari Ambassador's main targets for corruption were members of the Franco-Qatari Friendship Group in the French Parliament. They would be given Rolex watches or tokens for top department stores worth up to 5,000 or 6,000 euros each.

</div>
<div class="texte">

[123.] Kafka wrote *The Castle* in 1922, exactly one hundred years ago.

</div>
<div class="texte">

[124.] All seven patients admitted to hospital were suffering from kidney failure.

</div>
<div class="texte">

[125.] They were also suffering from serious sunstroke and were transferred into ICU (Intensive Care Unit) in a critical condition.

</div>
<div class="texte">

[126.] The wives of politicians corrupted by the Qatari Ambassador Mohammed al-Kuwari were likely to receive Vuitton handbags worth 5,000 euros each or other goods from leading brands.

</div>
<div class="texte">

[127.] June 2019, during the French heatwave. In Ille-et-Vilaine, an enquiry was opened into the death of a 33-year-old local roofer when the temperature was over 35 degrees centigrade in the shade.

</div>
<div class="texte">

[128.] Are you alive? Are you dead? Can you hear me? Can you see me?

</div>
<div class="texte">

[129.] Mohammed Jaham al-Kuwari offered luxury designer shoes to Nicolas Bays, French Socialist MP for the Nord-Pas-de-Calais constituency and a member of the France-Qatar Friendship Group in the French Parliament.

</div>
<div class="texte">

[130.] Please order sand-coloured body bags from now on, black body bags show up too easily (*memo*).

</div>
<div class="texte">

[131.] At his leaving party, Mohammed Jaham al-Kuwari received everyone he had corrupted, mainly from the worlds of politics and entertainment, while in office.

</div>
<div class="texte">

[132.] QDVC, a subsidiary of Vinci, was the first to have organised elections for workers' representatives, given the absence of trade unions, which are forbidden in Qatar.

</div>
<div class="texte">

[133.] In Doha, the National Museum of Qatar was designed by the French architect Jean Nouvel in the shape of a Desert Rose. Its cost is estimated at 434 million dollars.

</div>
<div class="texte">

[134.] Ultra-high performance fibre concrete is a new building material which achieves strengths six times higher than normal concrete together with similarly improved durability, thanks to the optimisation of the mix and the addition of metal fibre.

</div>
<div class="texte">

[135.] Biodegradable body bags in white Plastylon Link with front-mounted double-zipped closings.

</div>
<div class="texte">

[136.] Desert Rose Motif: 539 steel discs ranging from 14 to 87 metres diameter, clad in 115,000 square metres of ultra-high performance fibre concrete.

</div>
<div class="texte">

[137.] While admiring Jean Nouvel's Desert Rose design during the opening ceremony, I wondered how many workers had died on site.

</div>
<div class="texte">

[138.] A biodegradable material, Plastylon Link is in keeping with our times in that it respects the environment by fully decomposing in less than 8 months.

</div>
<div class="texte">

[139.] “My goal was to create an architecture in harmony with geography, in keeping with local tradition, a building able to bear maximum sunshine” wrote architect Jean Nouvel on his blog.

</div>
<div class="texte">

[140.] June 2019, French heatwave. In Cernay in the Haut-Rhin department, a 36-year-old man was found unconscious beside a digger.

</div>
<div class="texte">

[141.] Since the Nineties, there has been a quantum leap in our understanding of concrete materials with the development of new concepts in formulating cement mixes, using fibre and optimising granular stacking.

</div>
<div class="texte">

[142.] Plastylon Link emerged in 1989 as a biodegradable material for coffin-liners and burial bags. This material is typically used in coffins intended for cremation as it is flammable and sublimable.

</div>
<div class="texte">

[143.] “The National Museum of Qatar springs from desert which has ventured into the sea” wrote Jean Nouvel about his Desert Rose building.

</div>
<div class="texte">

[144.] During drinks held after the opening of the Desert Rose, architect Jean Nouvel and Sheikha Al-Mayassa, sister of the Emir and Chair of the Qatar Museums Authority, chatted together like old friends.

</div>
<div class="texte">

[145.] If used in a burial, Plastylon Link film is impermeable in water for up to one month and will then decay from hydrolysis or bacterial action, ensuring the decomposition of any corpse.

</div>
<div class="texte">

[146.] “Taking the Desert Rose as a departure point is becoming a very progressive, not to say a utopian idea,” writes Jean Nouvel.

</div>
<div class="texte">

[147.] In my airconditioned hotel room, I write airconditioned text-blocks: “A tall figure, always shaven-headed, always dressed in black, Jean Nouvel likes to talk about his latest baby.”

</div>
<div class="texte">

[148.] The building's skin is made of high-performance fibre-reinforced concrete, finished in a beige sand colour, used both inside and outside.

</div>
<div class="texte">

[149.] Jean Nouvel objects to the charge of working for non-democratic governments: “I work on a secular or trans-secular scale, for everyone, not for someone who happens to be in power”.

</div>
<div class="texte">

[150.] The body breaks down quickly as Plastylon Link contains very little plastic amongst mostly biodegradable materials.

</div>
<div class="texte">

[151.] During drinks after the opening of the Desert Rose, I saw Jean Nouvel and Jack Lang chatting together. I noted it, but I didn't use it in my article as it was an insignificant detail.

</div>
<div class="texte">

[152.] Our body bags are not talcum-powdered but are very soft and are therefore very pleasant to handle. Furthermore, the effect of the texture and white colour of the bag confers a certain decorum.

</div>
<div class="texte">

[153.] The UHPFRC (Ultra High-Performance Fibre-Reinforced Concrete) range allows for highly varied textures and satin-smooth facings in mat, brilliant, homogenous, and very consistent finishes with excellent aesthetic qualities.

</div>
<div class="texte">

[154.] Once the body bag and the corpse inside it have decomposed, no trace is left of either.

</div>
<div class="texte">

[155.] The Desert Rose is the first architecture self-created by nature, wind, spray, sand, and millennia, surprisingly complex and poetic.

</div>
<div class="texte">

[156.] Currently, we are working on a new model of biodegradable body bag which will speed up the decomposition of the body inside it, wiping out any trace in a few weeks.

</div>

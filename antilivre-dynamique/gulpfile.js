// Include Gulp
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    del = require('del'),
    cache = require('gulp-cache');
    gulpif = require('gulp-if');
    filter = require('gulp-filter'),
    imageResize = require('gulp-image-resize');
    changed = require("gulp-changed");
    rename = require("gulp-rename");
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec; // For running a local machine task


// Compress and transform all images
// gulp.task('images', function(done) {

//   [2000,1200,600,300].forEach(function (size) {
//     gulp.src( ['src/img/**/*.{png,gif,jpg}', '!src/img/**/*.mp4'])
//       .pipe(changed('static/img/'))
//       .pipe(imageResize({ width: size }))
//       .pipe(cache(imagemin([
//         imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
//         imagemin.jpegtran({progressive: true}),
//         imagemin.optipng({optimizationLevel: 5}),
//         imagemin.svgo({
//             plugins: [
//                 {removeViewBox: true},
//                 {cleanupIDs: false}
//             ]
//         })
//       ], {
//           verbose: true
//       }
//       )))
//       .pipe(gulpif(size == 2000, rename(function (path) { path.basename = `${path.basename}`; })))
//       .pipe(gulpif(size == 1200, rename(function (path) { path.basename = `${path.basename}-large`; })))
//       .pipe(gulpif(size == 600, rename(function (path) { path.basename = `${path.basename}-medium`; })))
//       .pipe(gulpif(size == 300, rename(function (path) { path.basename = `${path.basename}-small`; })))
//       .pipe(gulp.dest('static/img/'));
//     });
//   gulp.src( ['src/img/**/*.mp4'])
//      .pipe(gulp.dest('static/img/'));
//   done();
// });

gulp.task('images', () => {
  const f = filter(['src/img/**', '!src/img/favicon/*'], { restore: true });
  return gulp.src('src/img/**')
      .pipe(f)
      .pipe(cache(imagemin([
        imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
        imagemin.mozjpeg({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
       ], {verbose: true}
       )))
       .pipe(f.restore)
       .pipe(gulp.dest('static/img/'));
});

// Clean : clear cache and delete images
gulp.task('clean', function () {
  return del(['static/img/*']);
  cache.clearAll();
});

// Hugo
gulp.task(
  'hugo',
  gulp.series(
  function (cb) {
  exec("hugo --cleanDestinationDir --environment development", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

gulp.task(
  'hugo-prod',
  gulp.series(
  function (cb) {
  exec("hugo --minify --cleanDestinationDir --environment production", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "public-dev"
    },
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'content/**',
    'assets/**',
    'data/**',
    'layouts/**',
    'static/**',
    'archetypes/**',
    'config/**'
  ], gulp.series('hugo', reload));
  gulp.watch('src/img/**/*', gulp.series('images', reload));
});

gulp.task(
  'default',
  gulp.series('hugo', 'browser-sync', 'watch')
);

gulp.task(
  'deploy',
  gulp.series('hugo-prod', 'watch')
);
